# jfx-holamundo

Ejemplo básico de aplicación Java FX

## Requisitos previos
1. Tener JDK instalado y `PATH` apuntando a sus binarios (para que se encuentre `java` y `javac`)
2. Tener el SDK de JavaFX accesible (e.g: C:\JavaFX\javafx-sdk-11.0.2)

## Compilar código
Compilar especificando la ruta a los módulos de JavaFX y añadiendo el módulo javafx.controls
```
javac --module-path C:\JavaFX\javafx-sdk-11.0.2\lib --add-modules javafx.controls HelloFX.java
```

## Ejecutar aplicación
Ejecutar con la máquina virtual de Java especificando la ruta a los módulos de JavaFX y añadiendo el módulo javafx.controls
```
java --module-path C:\JavaFX\javafx-sdk-11.0.2\lib --add-modules javafx.controls HelloFX
```